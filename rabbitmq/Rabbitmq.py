import pika
import sys
def callback(ch,method,properties,body):
    print("Received %r"%(body,))

class Rabbitmq(object):

    def __init__(self,Rabbitmq_hostname):
        self.connection=pika.BlockingConnection(pika.ConnectionParameters(host=Rabbitmq_hostname))
        self.channel=self.connection.channel()
        self.result=self.channel.queue_declare(exclusive=True)
        self.queue_name=self.result.method.queue

    def publish(self,exchange_name,routing_Key,message):
        self.channel.exchange_declare(exchange=exchange_name,type='topic')
        self.channel.basic_publish(exchange=exchange_name,routing_key=routing_Key,body=message)
        self.connection.close()

    def consume(self,exchange_name,binding_Keys):
        self.channel.exchange_declare(exchange=exchange_name,type='topic')
        self.binding_keys=binding_Keys
        if not self.binding_keys:
            sys.stderr.wrtite("Usage:%s [binding_key]...\n"%sys.argv[0])
            sys.exit(0)

        for binding_key in self.binding_keys:
            self.channel.queue_bind(exchange=exchange_name,
                                    queue=self.queue_name,
                                    routing_key=binding_key)

        self.channel.basic_consume(callback,
                                   queue=self.queue_name,
                                   no_ack=True)
        self.channel.start_consuming()


if __name__=="__main__":
    dRMQ=Rabbitmq()
    dRMQ.publish('exchange_name','routing_Key','message')
    dRMQ.consume('exchange_name','binding_Keys')

