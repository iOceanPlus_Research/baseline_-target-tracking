#这是一个,protobuf序列化编码程序该版本是对之前版本进行的改进
import pymysql
import Ship_pb2 #protobuf编译后产生的py文件


class Serialization:
#连接数据库
    def getNewConnect(self,host,user,passwd,db):
        self.host=host
        self.user=user
        self.passwd=passwd
        self.db=db
        conn=pymysql.connect(host=self.host,user=self.user,passwd=self.passwd,db=self.db)
        cursor=conn.cursor()
        return cursor


    #conn = pymysql.connect(host='127.0.0.1', user='root', passwd='hbh18224511961', db='aisdb')
    #cursor = conn.cursor()
    def mysqlOperate(cur):
        cur.execute('select * from L1_Ship_History_Positions_ClassA')
        rows = cur.fetchall()
        position_tracks = Ship_pb2.L1_Ship_History_Position_Tracks()
        for row in rows:
            position = position_tracks.position.add()  # position_tracks中的position在proto中被定义为repeated所以就相当于创建一个集合,position是add方法返回的对象
            position.shipID = row[0]
            position.Belief_Ship_ID = row[1]
            position.Record_GPS_Time = row[2]
            position.Longitude = row[3]
            position.Latiude = row[4]
            position.Direction = row[5]
            position.Heading = row[6]
            position.Speed = row[7]
            position.Status = row[8]
            position.ROT_Sensor = row[9]
            position.AIS_Source = row[10]
            position_tracks.shipID = row[0]
           

        f = open("/Users/hubinhua/Desktop/python_out/ship.py/Data.txt", "wb")

        str=f.write(position_tracks.SerializeToString())

        f.close()
        cur.close()
        return str



cur=Serialization.getNewConnect('127.0.0.1', 'root','hbh18224511961','aisdb')
Serialization.mysqlOperate(cur)