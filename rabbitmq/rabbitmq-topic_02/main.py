import time
from client import TopicClient             #可能会出现相对地址错误,只要把整个文档放在一个独立工程里即可
from time import sleep
import threading

def callback(ch, method, properties, body):
    print(" [x] %r:%r" % (method.routing_key, body,))

#使用一个字符串做测试
def example_send(send_func):
    try:
        while True:
            print("Sending Message")
            send_func('test', 'test_topic')
            sleep(2)
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':


    print("Starting")

    threads = []

    tc = TopicClient()
    tc.consumer_to_topic(callback, 'test_topic')
    #创建两个线程,一个发数据,一个收数据
    p1 = threading.Thread(target=tc.run, args=())
    p1.start()
    threads.append(p1)

    p2 = threading.Thread(target=example_send, args=(tc.publish_to_topic,))
    p2.start()
    threads.append(p2)

    try:
        print(2)
        for thread in threads:
            thread.join()
            print(time.time())
    except KeyboardInterrupt:
        print("Keyboard interrupt in main")
    finally:
        print("Cleaning up Main")

#测试结果表明成功收发,每个两秒接收和发送一次数据